#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Parse the mount parameters
function add_mounts
{
	if [ "${1:-"rw"}" == 'rw' ]
	then
		MOUNT_HOME=( '--mount' "type=bind,source=${HOME},target=${HOME}" )
		local mountarr=( "${RW_MOUNTS[@]}" )
	else
		MOUNT_HOME=( '--mount' "type=bind,source=${HOME},target=${HOME},readonly" )
		local mountarr=( "${RO_MOUNTS[@]}" )
	fi
	
	for mountpoint in "${mountarr[@]}"
	do
		if [ -d "${mountpoint}" ]
		then
			VOLUMES+=( '--mount' "type=tmpfs,destination=${mountpoint}" )
			MOUNTS+=( "${mountpoint}" )
		else
			echo "${mountpoint} not available"
		fi
	done
	
	ENVS+=('--env' "WORKDIR=${PWD}")
}

# Implode the array by glue
function implode()
{
	local IFS="${1}"
	shift
	echo "${*}"
}

# Set up defaults
RW_MOUNTS=( "${HOME}/.ansible/tmp" "${HOME}/.ansible/cp" "${HOME}/.keychain" "${HOME}/.cache" )
RO_MOUNTS=( "${RW_MOUNTS[@]}" )

MOUNT_HOME=()
VOLUMES=()
MOUNTS=()
CONTAINER_TAG='latest'
ENVS=()

MOUNT_TYPE="${MOUNT_TYPE:-"none"}"

IMAGE_SOURCE="${IMAGE_SOURCE:-"quay.io/mireiawen/debian"}"

# Parse the command line
TEMP="$(getopt -n 'debian' -o 'mrut:e:f:v:i:' --long 'mount-home,mount-home-readonly,user-tag,tag:,env:,tmpfs:,volume:,image:' -- "${@}")"
eval set -- "${TEMP}"

while [ ${#} -gt 0 ]
do
	case "${1}" in
	'-m' | '--mount-home')
		CONTAINER_TAG="$(whoami)"
		MOUNT_TYPE='rw'
		shift
		;;
	
	'-r' | '--mount-home-readonly')
		CONTAINER_TAG="$(whoami)"
		MOUNT_TYPE='ro'
		shift
		;;
	
	'-u' | '--user-tag')
		CONTAINER_TAG="$(whoami)"
		shift
		;;
	
	'-t' | '--tag')
		CONTAINER_TAG="${2}"
		shift 2
		;;
	
	'-e' | '--env')
		ENVS+=( '--env' "${2}" )
		shift 2
		;;
	
	'-f' | '--tmpfs')
		RO_MOUNTS+=( "${2}" )
		RW_MOUNTS+=( "${2}" )
		shift 2
		;;

	'-v' | '--volume')
		VOLUMES+=( '--volume' "${2}" )
		if [ -n "${2#*:}" ]
		then
			volname="${2#*:}"
			if [ -n "${volname%%:*}" ]
			then
				volname="${volname%%:*}"
			fi
		else
			volname="${2}"
		fi
		MOUNTS+=( "${volname}" )
		shift 2
		;;

	'-i' | '--image')
		IMAGE_SOURCE="${2}"
		shift 2
		;;

	'--')
		shift
		break
		;;
	esac
done

# Set up the mounts if those are set
if [ "${MOUNT_TYPE}" != 'none' ]
then
	add_mounts "${MOUNT_TYPE}"
fi

# Add the mount points environment variable
ENVS+=( '--env' "VOLUMES=$(implode ';' "${MOUNTS[@]}")" )

# Mount the Docker socket if we can read it
if [ -r '/var/run/docker.sock' ]
then
	VOLUMES+=( '--volume' '/var/run/docker.sock:/var/run/docker.sock' )
fi

# And run the actual command
docker 'run' \
	--rm \
	--interactive \
	--tty \
	"${ENVS[@]}" \
	"${MOUNT_HOME[@]}" \
	"${VOLUMES[@]}" \
	"${IMAGE_SOURCE}:${CONTAINER_TAG}" \
	"${@}"
